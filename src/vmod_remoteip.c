/* This file is part of vmod_remoteip.
 * Copyright (C) 2020 Sergey Poznyakoff
 * 
 * Vmod_remoteip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * Vmod_remoteip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with vmod_remoteip.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <ctype.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <cache/cache.h>
#include <vcl.h>
#include <vcc_if.h>
#include <vsa.h>

static int
is_trusted_ip(VRT_CTX, VCL_ACL acl, char const *ipstr)
{
	struct addrinfo hints;
	struct addrinfo *res;
	struct suckaddr *ip;
	int rc;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_NUMERICHOST|AI_NUMERICSERV;

	rc = getaddrinfo(ipstr, NULL, &hints, &res);
	if (rc)
		return 0;

	ip = VSA_Malloc(res->ai_addr, res->ai_addrlen);
	freeaddrinfo(res);

	rc = VRT_acl_match(ctx, acl, ip);

	free(ip);
	
	return rc;
}	

VCL_STRING
vmod_get(VRT_CTX, VCL_ACL acl, VCL_STRING hdr)
{
	unsigned u;
	char const *end;
	char *buf;
	int i = 0;

	u = WS_ReserveAll(ctx->ws);
	buf = ctx->ws->f;
	
	end = hdr + strlen(hdr);
	while (end > hdr) {
		char const *p;
		size_t len, j;
		
		for (p = end - 1; p > hdr && *p != ','; p--)
			;
		len = end - p;
		AN(len + 1 < u);

		i = 0;
		j = 0;
		if (*p == ',')
			j++;
		while (j < len && isspace(p[j]))
			j++;
		while (j < len) {
			if (isspace(p[j]))
				break;
			buf[i++] = p[j++];
		}
		buf[i++] = 0;

		if (!is_trusted_ip(ctx, acl, buf)) {
			break;
		}
		end = p;
	}
	WS_Release(ctx->ws, i);
	return buf;
}
